# Notify Module
The "Notify Toaster" module provides an implementation of a toaster over top of the common "notify" api that is
defined in the "core" module.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-notify-toaster.git
```

Once the module is imported in the app, the following should be added to the HTML:
```
<!-- Notification -->
<toaster-container toaster-options="{'position-class': 'toast-bottom-left'}"></toaster-container>
```

Other options:

* Download the code at [http://code.tsstools.com/bower-notify-toaster/get/master.zip](http://code.tsstools.com/bower-notify-toaster/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-notify-toaster](http://code.tsstools.com/bower-notify-toaster)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.notify-toaster']);
```
